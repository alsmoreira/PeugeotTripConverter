#include "mainwindow.h"
#include <QApplication>
#include <QTranslator>
#include <QLibraryInfo>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QLocale locale = QLocale::system();
    if( locale.language() != QLocale::Brazil) {
      locale = QLocale("en_US");
      QLocale::setDefault(locale);
      QTranslator trans;
      if(trans.load("peugeottripconverter.qm"))
        app.installTranslator(&trans);
    }

    MainWindow w;

    w.show();

    return app.exec();
}
