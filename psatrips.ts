<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US" sourcelanguage="ru_RU">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <source>PSA Trips</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="457"/>
        <source>&amp;Открыть файл</source>
        <translation>&amp;Open file</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="384"/>
        <source>Действия</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="403"/>
        <source>;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="408"/>
        <source>,</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="413"/>
        <source>&lt;Tab&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="427"/>
        <source>с разделителем:</source>
        <translation>separator:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="443"/>
        <source>&amp;Экспорт в CSV</source>
        <translation>&amp;Export to CSV</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="98"/>
        <source>Автомобиль</source>
        <translation>The car</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="110"/>
        <source>VIN:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="129"/>
        <location filename="mainwindow.ui" line="164"/>
        <location filename="mainwindow.ui" line="215"/>
        <location filename="mainwindow.ui" line="266"/>
        <location filename="mainwindow.ui" line="301"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="142"/>
        <source>Остатка топлива(</source>
        <translation>Fuel remains (</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="180"/>
        <source>%)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="193"/>
        <source>Пробег:</source>
        <translation>Mileage:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="231"/>
        <location filename="mainwindow.ui" line="282"/>
        <location filename="mainwindow.ui" line="317"/>
        <location filename="mainwindow.ui" line="576"/>
        <location filename="mainwindow.ui" line="897"/>
        <source>км.</source>
        <translation>km.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="244"/>
        <source>До ТО:</source>
        <translation>Service:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="330"/>
        <source>Дата:</source>
        <translation>Date:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="365"/>
        <source>хватит на:</source>
        <translation>to drive:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="739"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="788"/>
        <source>N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="798"/>
        <source>Дата</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="803"/>
        <source>Дистанция, км</source>
        <translation>Distance, km</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="808"/>
        <source>В пути, ч:м</source>
        <translation>Travel time, h:m</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="813"/>
        <source>Скорость, км/ч</source>
        <translation>Avg. speed, kmh</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="818"/>
        <source>Топливо, л</source>
        <translation>Fuel, l</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="823"/>
        <source>Расход, л/100км</source>
        <translation>Avg. consump., l/100km</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1047"/>
        <source>Расход:</source>
        <translation>Fuel:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1355"/>
        <source>О программе</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="506"/>
        <source>Отбор</source>
        <translation>Selection</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="550"/>
        <source>Скрыть меньше</source>
        <translation>Don&apos;t show less than</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="563"/>
        <source>мин. или короче</source>
        <translation>min. or shorter</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="589"/>
        <location filename="mainwindow.ui" line="1347"/>
        <source>Обновить</source>
        <translation>Refresh</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="620"/>
        <source>Показать маршруты</source>
        <translation>Show routes</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="632"/>
        <source>нет</source>
        <translation>none</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="648"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="664"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="680"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="847"/>
        <source>Итого:</source>
        <translation>Summary:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="881"/>
        <source>Путь:</source>
        <translation>Dist.:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="932"/>
        <source>ч:м.</source>
        <translation>h:m.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="945"/>
        <source>Время:</source>
        <translation>Time:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="983"/>
        <source>км/ч.</source>
        <translation>kmh.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="996"/>
        <source>Ср.скор.:</source>
        <translation>Avg.speed:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1034"/>
        <source>л.</source>
        <translation>l.</translation>
    </message>
    <message>
        <source>Выпито:</source>
        <translation type="vanished">Fuel:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1085"/>
        <source>л/100км.</source>
        <translation>l/100km.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1098"/>
        <source>Ср.расход:</source>
        <translation>Avg.cons.:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1136"/>
        <source>#:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1152"/>
        <source>л/ч.</source>
        <translation>l/h.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1231"/>
        <source>Цена:</source>
        <translation>Fuel price:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1247"/>
        <source>Стоимость:</source>
        <translation>Total cost:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1263"/>
        <source>$</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1279"/>
        <source>$/л</source>
        <translation>$/l</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1301"/>
        <source>&amp;Файл</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1311"/>
        <source>Помощь</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1320"/>
        <source>Открыть</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1323"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1328"/>
        <source>Выход</source>
        <translation>Exit</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1331"/>
        <source>Alt+X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1339"/>
        <source>Экспорт</source>
        <translation>Export</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1342"/>
        <source>Ctrl+E</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1350"/>
        <source>Ctrl+R</source>
        <translation></translation>
    </message>
    <message>
        <source>Дистанция</source>
        <translation type="obsolete">Range</translation>
    </message>
    <message>
        <source>В пути</source>
        <translation type="obsolete">Time</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="793"/>
        <source>Маршрут</source>
        <translation>Route</translation>
    </message>
</context>
</TS>
