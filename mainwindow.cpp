#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <math.h>       /* round, floor, ceil, trunc, ok*/

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    int i;
    ui->setupUi(this);
    for( i=0; i<ui->tripsTAB->columnCount(); ++i) {
      ui->tripsTAB->horizontalHeaderItem(i)->setTextAlignment(Qt::AlignLeft);
      ui->tripsTAB->setColumnWidth(i, (i<=1)?50:((ui->tripsTAB->width()-ui->tripsTAB->verticalScrollBar()->width())/(ui->tripsTAB->columnCount()-2)) );
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_exitACT_triggered()
{
    close();
}


// Open trip files
void MainWindow::on_openfACT_triggered()
{
    QString fn = QFileDialog::getOpenFileName(this,"Select trips file",".","*.trips");
    if( fn.isEmpty())
        return;
    ui->fnLAB->setText(fn);
    ui->refreshACT->trigger();

    // Open actions
    ui->actionsBOX->setEnabled( true);
    ui->exportACT->setEnabled( true);
    ui->selectBOX->setEnabled( true);
}

// export
void MainWindow::on_exportACT_triggered()
{
    QString fn = QFileDialog::getSaveFileName(this,"Enter filename to export",".","*.csv");
    if( fn.isEmpty())
        return;
    QFile file(fn);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;
    int i=0, j=0;
    int imax=ui->tripsTAB->rowCount(), jmax=ui->tripsTAB->columnCount();
    int sep = ui->csvseparatorCMB->currentIndex();
    char separator[3] = {';',',','\t'};

    QTextStream out( &file);
    out.setCodec("cp1251");
    // headers
    for( j = 0; j<jmax; ++j) {
      out<<ui->tripsTAB->horizontalHeaderItem(j)->text();
      if((j+1)!=jmax)
        out<<separator[sep];
    }
//    out<<"ID\tRoute Date Distance TravelTime AverageSpeed FuelSpent AverageConsumption";

    for( i = 0; i<imax; ++i)
    {
      out<<'\n';
      for( j = 0; j<jmax; ++j){
        out<<ui->tripsTAB->item(i,j)->text();
        if((j+1)!=jmax)
          out<<separator[sep];
      }
    }
    file.close();
}


void MainWindow::on_openFileBUT_released()
{
  ui->openfACT->trigger();
}

void MainWindow::on_toCSVBUT_released()
{
  ui->exportACT->trigger();
}


void MainWindow::on_refreshBTN_released()
{
  ui->refreshACT->trigger();
}

void MainWindow::on_refreshACT_triggered()
{
  QString fn = ui->fnLAB->text();
  if( fn.isEmpty())
      return;
    QJsonParseError jse;
    QFile tripsfile(fn);
    if( !tripsfile.open(QIODevice::ReadOnly|QIODevice::Text))
    {
      QMessageBox::critical(this,"Error","File open error",QMessageBox::Ok);
      return;
    }

    QJsonDocument trips = QJsonDocument::fromJson(tripsfile.readAll() ,&jse);
    if( jse.error!=QJsonParseError::NoError || trips.isEmpty() || !trips.isObject())
    {
      QMessageBox::critical(this,"Error",QString("File parse error:\n")+jse.errorString(),QMessageBox::Ok);
      return;
    }

    QJsonObject tripsobj = trips.object(); // objetc json root
    ui->vinLAB->setText( tripsobj["vin"].toString() ); // vin

    QJsonObject vehobj = tripsobj["vehicleinformation"].toObject(); // vehicleinformation json a object
    ui->distLAB->setText( QString::number( vehobj["mileage"].toInt() / 10) ); // mileage
    ui->fuelLAB->setText( QString::number( vehobj["fuelautonomy"].toInt()) ); // fuel km
    ui->fuelprLAB->setText( QString::number( vehobj["fuellevel"].toInt()) ); // fuel %
    int toRange = vehobj["distancetonextmaintenance"].toInt();
    if(toRange > 1000)
        ui->toLAB->setStyleSheet("color: green;");
    else if(toRange > 0)
        ui->toLAB->setStyleSheet("color: #ffaaaa;");
    else if(toRange < 0)
        ui->toLAB->setStyleSheet("color: red;");
    ui->toLAB->setText( QString::number(toRange) ); // до ТО
    int updTime = vehobj["updatedon"].toInt();
    ui->updateTME->setDateTime( QDateTime::fromTime_t(updTime)); // readout time
// list of trips
    QJsonArray tripsarr = tripsobj["trips"].toArray();
  //  QMessageBox::information(this,"N of trips",QString::number(tripsarr.count()));
    if( tripsarr.isEmpty() )
        return;
    // clearing a table
    ui->tripsTAB->clearContents();
    // deleting table rows
    while( ui->tripsTAB->rowCount())
      ui->tripsTAB->removeRow( 0);
// default sort
    ui->tripsTAB->sortItems(0,Qt::DescendingOrder);
// statistical variables
    int allDist=0, allFuel=0, allTime=0; // суммы м, мл, мин
// helper variables
    const QCheckBox* routesCHK[4] = {ui->norouteCHK, ui->route1CHK, ui->route2CHK, ui->route3CHK}; // array of daws for display of routes
    int i=0; // counter
    for(; i<tripsarr.count(); ++i)
    { // add a trip
      QJsonObject atrip = tripsarr[i].toObject();
      if(atrip.isEmpty())
          continue;

      int route = atrip["typ"].toInt(); // route, variable for reading values
      int tripTime = atrip["tme"].toInt(); // В ways, minutes
      int distlen = round(atrip["dst"].toDouble()); // The distance can be fractional!!!
      updTime = atrip["dte"].toInt(); // date
      if( tripTime < ui->minuteSPN->value()) // travel time selection
        continue;
      if( distlen < ui->kmSPN->value()*1000) // selection by distance
        continue;
      if(route<4 && !routesCHK[route]->isChecked())
        continue; // selection by route number
      if( ui->fromTME->dateTime() > QDateTime::fromTime_t(updTime) || ui->toTME->dateTime() < QDateTime::fromTime_t(updTime))
        continue; // selection by route number

      ui->tripsTAB->insertRow(0); // new empty string

      QTableWidgetItem *item = new QTableWidgetItem;
      item->setData( Qt::EditRole, atrip["id"].toInt() );
      ui->tripsTAB->setItem(0,0,item); // id

      //route = atrip["typ"].toInt(); // route
      item = new QTableWidgetItem;
      route ? item->setData( Qt::EditRole, route) : item->setData( Qt::EditRole, "-");
      switch(route){ // cell coloring by route number
      case 1: item->setBackgroundColor( QColor(0x99,0x99,0x99));
        break;
      case 2: item->setBackgroundColor( QColor(0xAA,0xAA,0x33));
        break;
      case 3: item->setBackgroundColor( QColor(0xAA,0x33,0x33));
        break;
      }
      ui->tripsTAB->setItem(0,1,item); // route

      item = new QTableWidgetItem;
      item->setData( Qt::EditRole, QDateTime::fromTime_t(updTime) );
      ui->tripsTAB->setItem(0,2,item);

//      int distlen = atrip["dst"].toInt(); // Distance
      allDist += distlen;
      item = new QTableWidgetItem;
      item->setData( Qt::EditRole,double(distlen)/1000. );
      ui->tripsTAB->setItem(0,3,item);

//      tripTime = atrip["tme"].toInt(); // In the way, minutes
      allTime += tripTime;
      item = new QTableWidgetItem( QString("%1").arg(tripTime/60, 2, 10, QChar('0')) + ":" + QString("%1").arg(tripTime%60, 2, 10, QChar('0')) );
      ui->tripsTAB->setItem(0,4,item);

      // скорость
      item = new QTableWidgetItem;
      item->setData(Qt::EditRole,(distlen>0 && tripTime>0)?(round( double( distlen*60 / tripTime) / 100.) / 10.):0 );
      ui->tripsTAB->setItem(0,5,item);

      double fuel = atrip["vol"].toDouble(); // fuel
      allFuel += round(fuel * 1000.);
      item = new QTableWidgetItem;
      item->setData(Qt::EditRole, round(fuel * 1000.) / 1000. );
      ui->tripsTAB->setItem(0,6,item); // fuel

      if( distlen>0) // average consumption
        fuel = fuel / (double(distlen) / 100000.);
      item = new QTableWidgetItem;
      if(distlen>0)
        item->setData( Qt::EditRole, round(fuel * 1000.) / 1000.);
      else
        item->setData( Qt::EditRole, "-");
      ui->tripsTAB->setItem(0,7,item); // consumption
    }
// Filling in the totals
    ui->noftripsLAB->setText( QString::number(ui->tripsTAB->rowCount()));
    ui->allDistLAB->setText( QString::number(double(allDist)/1000.,'f',1));
    ui->allTimeLAB->setText( QString("%1").arg(allTime/60, 2, 10, QChar('0')) + ":" + QString("%1").arg(allTime%60, 2, 10, QChar('0')));
    ui->allFuelLAB->setText( QString::number(double(allFuel)/1000.,'f',1));
    if( allTime>0 && allDist>0) {
      ui->avgSpeedLAB->setText( QString::number(double(allDist)*60./1000./double(allTime),'f',1));
      ui->avgConsLAB->setText( QString::number(double(allFuel)/(double(allDist)/100.),'f',1));
      ui->avgTConsLAB->setText( QString::number( double(allFuel)*60./double(allTime)/1000.,'f',1));
    } else {
        ui->avgSpeedLAB->setText( "0");
        ui->avgConsLAB->setText(  "0");
        ui->avgTConsLAB->setText( "0");
    }
    ui->priceBOX->valueChanged(ui->priceBOX->value());
}

void MainWindow::on_priceBOX_valueChanged(double arg1)
{
  ui->costLAB->setText( QString::number( arg1 * ui->allFuelLAB->text().toDouble(),'f',2));
}

void MainWindow::on_aboutACT_triggered()
{
    QMessageBox::about(NULL,"About PeugeotTripConverter","Freeware software\nOriginal Author: Roman St.Zhavnis\nModify by: Anderson Moreira");
}
