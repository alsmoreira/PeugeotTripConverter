#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QFileDialog>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QMessageBox>
#include <QByteArray>
#include <QDateTime>
#include <QDateTimeEdit>
#include <QTextStream>
#include <QTableWidgetItem>
#include <QScrollBar>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:

private slots:
    void on_exitACT_triggered();

    void on_openfACT_triggered();

    void on_exportACT_triggered();
    void on_openFileBUT_released();

    void on_toCSVBUT_released();

    void on_refreshBTN_released();

    void on_refreshACT_triggered();

    void on_aboutACT_triggered();

    void on_priceBOX_valueChanged(double arg1);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
