#ifndef TRIPSTABITEM
#define TRIPSTABITEM

#include <QTableWidgetItem>

class TripsTabItem: public QTableWidgetItem
{
public:
    bool operator <(const QTableWidgetItem &other) const
    {
        return text().toInt() < other.text().toInt();
    }
};

#endif // TRIPSTABITEM

